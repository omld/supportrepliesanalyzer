package ua.omld.qu.task.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServiceTest {

	private static final int TOO_HIGH_ID = 11;
	private static final int TOO_LOW_ID = 0;
	private static final int GOOD_ID = 1;
	private static final int TOO_HIGH_VARIATION = 4;
	private static final int TOO_LOW_VARIATION = 0;
	private static final int GOOD_VARIATION = 1;
	private static final int MATCH_ALL_VALUE = 0;

	@Test
	public void testIsMatch_whenOtherIsNull_returnFalse() {
		Service other = null;
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertFalse(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherIsSame_returnFalse() {
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertTrue(service.isMatch(service));
	}

	@Test
	public void testIsMatch_whenOtherMatchAll1_returnTrue() {
		Service other = Service.getService();
		Service service = Service.getService(GOOD_ID);

		assertTrue(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherMatchAll2_returnTrue() {
		Service other = Service.getService();
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertTrue(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherWithSameId_returnTrue() {
		Service other = Service.getService(GOOD_ID);
		Service service = Service.getService(GOOD_ID);

		assertTrue(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherWithSameIdAndNoVariation_returnTrue() {
		Service other = Service.getService(GOOD_ID);
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertTrue(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherWithOtherIdAndNoVariation_returnFalse() {
		Service other = Service.getService(2);
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertFalse(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherWithSameIdAndVariation_returnTrue() {
		Service other = Service.getService(GOOD_ID, GOOD_VARIATION);
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertTrue(service.isMatch(other));
	}

	@Test
	public void testIsMatch_whenOtherWithOtherIdAndSameVariation_returnFalse() {
		Service other = Service.getService(2, GOOD_VARIATION);
		Service service = Service.getService(GOOD_ID, GOOD_VARIATION);

		assertFalse(service.isMatch(other));
	}

	@Test
	public void testGetService_whenCall_getMatchAllService() {
		final int prime = 31;
		int expectedHash = 1;
		expectedHash = prime * expectedHash + MATCH_ALL_VALUE;
		expectedHash = prime * expectedHash + MATCH_ALL_VALUE;

		Service service = Service.getService();
		int actualHash = service.hashCode();

		assertEquals(expectedHash, actualHash);
	}

	@Test
	public void testGetService_whenMoreThenOneCall_returnSameInstance() {
		Service serviceOne = Service.getService();
		Service serviceTwo = Service.getService();

		assertTrue(serviceOne == serviceTwo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceInteger_whenIdAbove_throwsException() {
		Service.getService(TOO_HIGH_ID);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceInteger_whenIdBelow_throwsException() {
		Service.getService(TOO_LOW_ID);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceInteger_whenIdMatchAll_throwsException() {
		Service.getService(MATCH_ALL_VALUE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceIntegerInteger_whenIdAbove_throwsException() {
		Service.getService(TOO_HIGH_ID, GOOD_VARIATION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceIntegerInteger_whenIdBelow_throwsException() {
		Service.getService(TOO_LOW_ID, GOOD_VARIATION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceIntegerInteger_whenIdMatchAll_throwsException() {
		Service.getService(MATCH_ALL_VALUE, GOOD_VARIATION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceIntegerInteger_whenVariationAbove_throwsException() {
		Service.getService(GOOD_ID, TOO_HIGH_VARIATION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceIntegerInteger_whenVariationBelow_throwsException() {
		Service.getService(GOOD_ID, TOO_LOW_VARIATION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetServiceIntegerInteger_whenVariationMatchAll_throwsException() {
		Service.getService(GOOD_ID, MATCH_ALL_VALUE);
	}

}
