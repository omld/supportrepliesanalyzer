package ua.omld.qu.task.testdata;

import java.time.LocalDate;

import ua.omld.qu.task.model.Question;
import ua.omld.qu.task.model.ResponseType;
import ua.omld.qu.task.model.Service;
import ua.omld.qu.task.model.SupportReply;

public enum TestReplies {

    REPLY_1(Service.getService(1, 1), Question.getQuestion(8, 15, 1), ResponseType.FIRST, LocalDate.of(2012, 10, 15),
            83),
    REPLY_2(Service.getService(1), Question.getQuestion(10, 1), ResponseType.FIRST, LocalDate.of(2012, 12, 1), 65),
    REPLY_3(Service.getService(1, 1), Question.getQuestion(5, 5, 1), ResponseType.FIRST, LocalDate.of(2012, 11, 1),
            117),
    REPLY_4(Service.getService(3), Question.getQuestion(10, 2), ResponseType.NEXT, LocalDate.of(2012, 10, 2), 100);

    SupportReply reply;

    private TestReplies(Service service, Question question, ResponseType responseType, LocalDate date,
            Integer watingTime) {
        reply = new SupportReply(service, question, responseType, date, watingTime);
    }

    public SupportReply getReply() {
        return reply;
    }

    public SupportReply getCopy() {
        return new SupportReply(reply.getService(), reply.getQuestion(), reply.getResponseType(), reply.getDate(),
                reply.getWaitingTime());
    }

}
