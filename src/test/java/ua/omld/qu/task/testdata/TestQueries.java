package ua.omld.qu.task.testdata;

import java.time.LocalDate;

import ua.omld.qu.task.model.Query;
import ua.omld.qu.task.model.Question;
import ua.omld.qu.task.model.ResponseType;
import ua.omld.qu.task.model.Service;

public enum TestQueries {

    QUERY_1(Service.getService(1, 1), Question.getQuestion(8), ResponseType.FIRST, LocalDate.of(2012, 1, 1),
            LocalDate.of(2012, 12, 01)),
    QUERY_2(Service.getService(1), Question.getQuestion(), ResponseType.FIRST, LocalDate.of(2012, 10, 8),
            LocalDate.of(2012, 11, 20)),
    QUERY_3(Service.getService(3), Question.getQuestion(10), ResponseType.FIRST, LocalDate.of(2012, 12, 1), null);

    private Query query;

    private TestQueries(Service service, Question question, ResponseType responseType, LocalDate startDate,
            LocalDate endDate) {
        query = new Query(service, question, responseType, startDate, endDate);
    }

    public Query getQuery() {
        return query;
    }

    public Query getCopy() {
        return new Query(query.getService(), query.getQuestion(), query.getResponseType(), query.getStartDate(),
                query.getEndDate());
    }

}
