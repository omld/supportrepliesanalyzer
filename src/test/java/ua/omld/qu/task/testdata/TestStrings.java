package ua.omld.qu.task.testdata;

public class TestStrings {

    public static final String REPLY_1_STRING = "C 1.1 8.15.1 P 15.10.2012 83" + System.lineSeparator();
    public static final String REPLY_2_STRING = "C 1 10.1 P 01.12.2012 65" + System.lineSeparator();
    public static final String REPLY_3_STRING = "C 1.1 5.5.1 P 01.11.2012 117" + System.lineSeparator();
    public static final String REPLY_4_STRING = "C 3 10.2 N 02.10.2012 100" + System.lineSeparator();

    public static final String QUERY_1_STRING = "D 1.1 8 P 01.01.2012-01.12.2012" + System.lineSeparator();
    public static final String QUERY_2_STRING = "D 1 * P 08.10.2012-20.11.2012" + System.lineSeparator();
    public static final String QUERY_3_STRING = "D 3 10 P 01.12.2012" + System.lineSeparator();

    public static final String BAD_REPLY_1_STRING = "C * 10.1 P 01.12.2012 65" + System.lineSeparator();
    public static final String BAD_REPLY_2_STRING = "C 1.1 5.5.1 P 01.11.2012 1T7" + System.lineSeparator();

    public static final String BAD_QUERY_1_STRING = "D 1.1 8 F 01.01.2012-01.12.2012" + System.lineSeparator();

    private TestStrings() {
    }

}
