package ua.omld.qu.task.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.junit.Test;

import ua.omld.qu.task.exception.DataReaderException;
import ua.omld.qu.task.model.Query;
import ua.omld.qu.task.model.SupportReply;
import ua.omld.qu.task.testdata.TestQueries;
import ua.omld.qu.task.testdata.TestReplies;
import ua.omld.qu.task.testdata.TestStrings;

public class DataReaderTest {

    @Test(expected = DataReaderException.class)
    public void testParseData_badLinesCount() throws IOException {
        String linesCountString = "One" + System.lineSeparator();
        String readerSource = linesCountString;
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();
    }

    @Test(expected = DataReaderException.class)
    public void testParseData_badReply1() throws IOException {
        String linesCountString = 1 + System.lineSeparator();
        String readerSource = linesCountString.concat(TestStrings.BAD_REPLY_1_STRING);
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();
    }

    @Test(expected = DataReaderException.class)
    public void testParseData_badReply2() throws IOException {
        String linesCountString = 2 + System.lineSeparator();
        String readerSource = linesCountString.concat(TestStrings.REPLY_1_STRING)
                .concat(TestStrings.BAD_REPLY_1_STRING);
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();
    }

    @Test(expected = DataReaderException.class)
    public void testParseData_badQuery() throws IOException {
        String linesCountString = 1 + System.lineSeparator();
        String readerSource = linesCountString.concat(TestStrings.BAD_QUERY_1_STRING);
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();
    }

    @Test
    public void testParseData_oneReply() throws IOException {
        SupportReply expectedReply = TestReplies.REPLY_1.getReply();
        String linesCountString = 1 + System.lineSeparator();
        String readerSource = linesCountString.concat(TestStrings.REPLY_1_STRING);
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();

        assertEquals(1, dataReader.getParsedSupportReplies().size());
        assertEquals(0, dataReader.getParsedQueries().size());
        SupportReply actualReply = dataReader.getParsedSupportReplies().first();
        assertEquals(expectedReply, actualReply);
    }

    @Test
    public void testParseData_oneQuery() throws IOException {
        Query expectedQuery = TestQueries.QUERY_1.getQuery();
        String linesCountString = 1 + System.lineSeparator();
        String readerSource = linesCountString.concat(TestStrings.QUERY_1_STRING);
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();

        assertEquals(0, dataReader.getParsedSupportReplies().size());
        assertEquals(1, dataReader.getParsedQueries().size());
        Query actualQuery = dataReader.getParsedQueries().first();
        assertEquals(expectedQuery, actualQuery);
    }

    @Test
    public void testParseData_complex() throws IOException {
        NavigableSet<SupportReply> expectedReplies = new TreeSet<>();
        expectedReplies.add(TestReplies.REPLY_1.getCopy());
        expectedReplies.add(TestReplies.REPLY_2.getCopy());
        expectedReplies.add(TestReplies.REPLY_3.getCopy());
        expectedReplies.add(TestReplies.REPLY_4.getCopy());
        NavigableSet<Query> expectedQueries = new TreeSet<>();
        expectedQueries.add(TestQueries.QUERY_1.getCopy());
        expectedQueries.add(TestQueries.QUERY_2.getCopy());
        expectedQueries.add(TestQueries.QUERY_3.getCopy());
        String linesCountString = 7 + System.lineSeparator();
        String readerSource = linesCountString
                .concat(TestStrings.REPLY_1_STRING)
                .concat(TestStrings.REPLY_2_STRING)
                .concat(TestStrings.REPLY_3_STRING)
                .concat(TestStrings.QUERY_1_STRING)
                .concat(TestStrings.REPLY_4_STRING)
                .concat(TestStrings.QUERY_2_STRING)
                .concat(TestStrings.QUERY_3_STRING)
                ;
        StringReader reader = new StringReader(readerSource);
        DataReader dataReader = new DataReader(reader);

        dataReader.parseData();
        dataReader.close();

        NavigableSet<SupportReply> actualReplies = dataReader.getParsedSupportReplies();
        NavigableSet<Query> actualQueries = dataReader.getParsedQueries();
        assertArrayEquals(expectedReplies.toArray(), actualReplies.toArray());
        assertArrayEquals(expectedQueries.toArray(), actualQueries.toArray());
    }

}
