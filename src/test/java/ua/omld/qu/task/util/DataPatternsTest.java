package ua.omld.qu.task.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataPatternsTest {

    @Test
    public void testQuestionPattern_onlyQuestionType_success() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("1");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("5");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("9");
        Matcher matcher4 = DataPatterns.QUESTION_PATTERN.matcher("10");
        Matcher matcher5 = DataPatterns.QUESTION_PATTERN.matcher("*");
        assertTrue("`1` must match", matcher1.matches());
        assertTrue("`5` must match", matcher2.matches());
        assertTrue("`9` must match", matcher3.matches());
        assertTrue("`10` must match", matcher4.matches());
        assertTrue("`*` must match", matcher5.matches());
    }

    @Test
    public void testQuestionPattern_onlyQuestionType_wrongQuestionId() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("0");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("11");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("-11");
        Matcher matcher4 = DataPatterns.QUESTION_PATTERN.matcher("a5");
        assertFalse("`0` must not match", matcher1.matches());
        assertFalse("`11` must not match", matcher2.matches());
        assertFalse("`-11` must not match", matcher3.matches());
        assertFalse("`a5` must not match", matcher4.matches());
    }

    @Test
    public void testQuestionPattern_onlyQuestionTypeAndCategory_success() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("1.1");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("5.2");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("9.3");
        Matcher matcher4 = DataPatterns.QUESTION_PATTERN.matcher("10.3");
        assertTrue("`1.1` must match", matcher1.matches());
        assertTrue("`5.2` must match", matcher2.matches());
        assertTrue("`9.3` must match", matcher3.matches());
        assertTrue("`10.3` must match", matcher4.matches());
    }

    @Test
    public void testQuestionPattern_onlyQuestionTypeAndCategory_wrongQuestionType() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("0.1");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("11.10");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("-11.19");
        Matcher matcher4 = DataPatterns.QUESTION_PATTERN.matcher("*.1");
        assertFalse("`0.1` must not match", matcher1.matches());
        assertFalse("`11.2` must not match", matcher2.matches());
        assertFalse("`-11.3` must not match", matcher3.matches());
        assertFalse("`*.1` must not match", matcher4.matches());
    }

    @Test
    public void testQuestionPattern_onlyQuestionTypeAndCategory_wrongCategory() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("1.0");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("5.21");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("9.");
        Matcher matcher4 = DataPatterns.QUESTION_PATTERN.matcher("*.");
        assertFalse("`1.0` must not match", matcher1.matches());
        assertFalse("`5.21` must not match", matcher2.matches());
        assertFalse("`9.` must not match", matcher3.matches());
        assertFalse("`*.` must not match", matcher4.matches());
    }

    @Test
    public void testQuestionPattern_questionTypeCategorySubcategory_success() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("1.1.1");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("5.8.2");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("9.13.3");
        Matcher matcher4 = DataPatterns.QUESTION_PATTERN.matcher("10.20.3");
        assertTrue("`1.1.1` must match", matcher1.matches());
        assertTrue("`5.8.2` must match", matcher2.matches());
        assertTrue("`9.13.3` must match", matcher3.matches());
        assertTrue("`10.20.3` must match", matcher4.matches());
        assertEquals("5", matcher2.group(1));
        assertEquals("8", matcher2.group(2));
        assertEquals("2", matcher2.group(3));
        assertEquals("10", matcher4.group(1));
        assertEquals("20", matcher4.group(2));
        assertEquals("3", matcher4.group(3));
    }

    @Test
    public void testQuestionPattern_questionTypeCategorySubcategory_wrongCategory() {
        Matcher matcher1 = DataPatterns.QUESTION_PATTERN.matcher("1.1.0");
        Matcher matcher2 = DataPatterns.QUESTION_PATTERN.matcher("5.8.4");
        Matcher matcher3 = DataPatterns.QUESTION_PATTERN.matcher("9.16.");
        assertFalse("`1.1.0` must not match", matcher1.matches());
        assertFalse("`5.8.4` must not match", matcher2.matches());
        assertFalse("`9.16.` must not match", matcher3.matches());
    }

    @Test
    public void testServicePattern_onlyServiceId_oneDigit() {
        Matcher matcher1 = DataPatterns.SERVICE_PATTERN.matcher("1");
        Matcher matcher2 = DataPatterns.SERVICE_PATTERN.matcher("5");
        Matcher matcher3 = DataPatterns.SERVICE_PATTERN.matcher("9");
        Matcher matcher4 = DataPatterns.SERVICE_PATTERN.matcher("*");
        assertTrue("`1` must match", matcher1.matches());
        assertTrue("`5` must match", matcher2.matches());
        assertTrue("`9` must match", matcher3.matches());
        assertTrue("`*` must match", matcher4.matches());
    }

    @Test
    public void testServicePattern_onlyServiceId_twoDigit() {
        Matcher matcher1 = DataPatterns.SERVICE_PATTERN.matcher("10");
        assertTrue("`10` must match", matcher1.matches());
    }

    @Test
    public void testServicePattern_onlyServiceId_wrongServiceId() {
        Matcher matcher1 = DataPatterns.SERVICE_PATTERN.matcher("0");
        Matcher matcher2 = DataPatterns.SERVICE_PATTERN.matcher("11");
        Matcher matcher3 = DataPatterns.SERVICE_PATTERN.matcher("-11");
        assertFalse("`0` must not match", matcher1.matches());
        assertFalse("`11` must not match", matcher2.matches());
        assertFalse("`-11` must not match", matcher3.matches());
    }

    @Test
    public void testServicePattern_serviceIdOneDigit_variationIdOneDigit() {
        Matcher matcher1 = DataPatterns.SERVICE_PATTERN.matcher("1.1");
        Matcher matcher2 = DataPatterns.SERVICE_PATTERN.matcher("5.2");
        Matcher matcher3 = DataPatterns.SERVICE_PATTERN.matcher("9.3");
        assertTrue("`1` must match", matcher1.matches());
        assertTrue("`5` must match", matcher2.matches());
        assertTrue("`9` must match", matcher3.matches());
    }

    @Test
    public void testServicePattern_wrongServiceId() {
        Matcher matcher1 = DataPatterns.SERVICE_PATTERN.matcher("0.1");
        Matcher matcher2 = DataPatterns.SERVICE_PATTERN.matcher("11.2");
        Matcher matcher3 = DataPatterns.SERVICE_PATTERN.matcher("-11.3");
        Matcher matcher4 = DataPatterns.SERVICE_PATTERN.matcher("*.3");
        assertFalse("`0.1` must not match", matcher1.matches());
        assertFalse("`11.2` must not match", matcher2.matches());
        assertFalse("`-11.3` must not match", matcher3.matches());
        assertFalse("`*.3` must not match", matcher4.matches());
    }

    @Test
    public void testServicePattern_wrongVariationId() {
        Matcher matcher1 = DataPatterns.SERVICE_PATTERN.matcher("1.0");
        Matcher matcher2 = DataPatterns.SERVICE_PATTERN.matcher("5.4");
        Matcher matcher3 = DataPatterns.SERVICE_PATTERN.matcher("9.");
        assertFalse("`1.0` must not match", matcher1.matches());
        assertFalse("`5.4` must not match", matcher2.matches());
        assertFalse("`9.` must not match", matcher3.matches());
    }

    @Test
    public void testDatePattern_wrongDate() {
        Matcher matcher1 = DataPatterns.DATE_PATTERN.matcher("01.01.12");
        Matcher matcher2 = DataPatterns.DATE_PATTERN.matcher("05.14.2012");
        Matcher matcher3 = DataPatterns.DATE_PATTERN.matcher("9.08.2012");
        assertFalse("`01.01.12` must not match", matcher1.matches());
        assertFalse("`05.14.2012` must not match", matcher2.matches());
        assertFalse("`9.08.2012` must not match", matcher3.matches());
    }

    @Test
    public void testDatePattern_success() {
        Matcher matcher1 = DataPatterns.DATE_PATTERN.matcher("01.01.2012");
        Matcher matcher2 = DataPatterns.DATE_PATTERN.matcher("31.12.2012");
        Matcher matcher3 = DataPatterns.DATE_PATTERN.matcher("05.08.2012");
        assertTrue("`01.01.2012` must match", matcher1.matches());
        assertTrue("`31.12.2012` must match", matcher2.matches());
        assertTrue("`05.08.2012` must match", matcher3.matches());
    }

    @Test
    public void testDatesRangePattern_success() {
        Matcher matcher1 = DataPatterns.DATES_RANGE_PATTERN.matcher("01.01.2012");
        Matcher matcher2 = DataPatterns.DATES_RANGE_PATTERN.matcher("01.01.2012-31.12.2012");
        assertTrue("`01.01.2012` must match", matcher1.matches());
        assertTrue("`01.01.2012-31.12.2012` must match", matcher2.matches());
    }

    @Test
    public void testDatesRangePattern_wrong() {
        Matcher matcher1 = DataPatterns.DATES_RANGE_PATTERN.matcher("-01.01.2012");
        Matcher matcher2 = DataPatterns.DATES_RANGE_PATTERN.matcher("01.01.2012-31.12.12");
        Matcher matcher3 = DataPatterns.DATES_RANGE_PATTERN.matcher("05.08.2012-");
        Matcher matcher4 = DataPatterns.DATES_RANGE_PATTERN.matcher("01.01.2012-05.08.2012-31.12.12");
        assertFalse("`-01.01.2012` must not match", matcher1.matches());
        assertFalse("`01.01.2012-31.12.12` must not match", matcher2.matches());
        assertFalse("`05.08.2012-` must not match", matcher3.matches());
        assertFalse("`01.01.2012-05.08.2012-31.12.12` must not match", matcher4.matches());
    }

}
