package ua.omld.qu.task.logic;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import ua.omld.qu.task.model.Query;
import ua.omld.qu.task.model.SupportReply;
import ua.omld.qu.task.testdata.TestQueries;
import ua.omld.qu.task.testdata.TestReplies;

public class AnalyzerTest {

    private Analyzer analyzer = new Analyzer();
    private NavigableSet<SupportReply> supportReplies;
    private NavigableSet<Query> queries;
    private List<Integer> expectedStatistics;

    @Before
    public void setUp() {
        supportReplies = new TreeSet<>();
        queries = new TreeSet<>();
        expectedStatistics = new ArrayList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAnalyze_noInputData_exception() {
        analyzer.analyze(supportReplies, queries);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAnalyze_noInputQueries_exception() {
        supportReplies.add(TestReplies.REPLY_1.getReply());

        analyzer.analyze(supportReplies, queries);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAnalyze_noInputReplies_exception() {
        queries.add(TestQueries.QUERY_1.getQuery());

        analyzer.analyze(supportReplies, queries);
    }

    @Test
    public void testAnalyze_query1Case1_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        queries.add(TestQueries.QUERY_1.getCopy());
        expectedStatistics.add(TestReplies.REPLY_1.getReply().getWaitingTime());

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query1Case2_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        queries.add(TestQueries.QUERY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        expectedStatistics.add(TestReplies.REPLY_1.getReply().getWaitingTime());

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query1Case3_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        queries.add(TestQueries.QUERY_1.getCopy());
        expectedStatistics.add(TestReplies.REPLY_1.getReply().getWaitingTime());

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query2Case1_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        expectedStatistics.add(TestReplies.REPLY_1.getReply().getWaitingTime());

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query2Case2_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        expectedStatistics.add(TestReplies.REPLY_1.getReply().getWaitingTime());

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query2Case3_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        Integer averageTime = (TestReplies.REPLY_1.getReply().getWaitingTime()
                + TestReplies.REPLY_3.getReply().getWaitingTime()) / 2;
        expectedStatistics.add(averageTime);

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query2Case4_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        Integer averageTime = (TestReplies.REPLY_1.getReply().getWaitingTime()
                + TestReplies.REPLY_3.getReply().getWaitingTime()) / 2;
        expectedStatistics.add(averageTime);

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query3Case1_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        queries.add(TestQueries.QUERY_3.getCopy());
        expectedStatistics.add(null);

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query1Query2Query3Case1_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        queries.add(TestQueries.QUERY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        queries.add(TestQueries.QUERY_3.getCopy());
        Integer averageTime1 = TestReplies.REPLY_1.getReply().getWaitingTime();
        Integer averageTime2 = (TestReplies.REPLY_1.getReply().getWaitingTime()
                + TestReplies.REPLY_3.getReply().getWaitingTime()) / 2;
        Integer averageTime3 = null;
        expectedStatistics.add(averageTime1);
        expectedStatistics.add(averageTime2);
        expectedStatistics.add(averageTime3);

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

    @Test
    public void testAnalyze_query1Query2Query3Case2_success() {
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_2.getCopy());
        supportReplies.add(TestReplies.REPLY_3.getCopy());
        queries.add(TestQueries.QUERY_1.getCopy());
        supportReplies.add(TestReplies.REPLY_4.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        queries.add(TestQueries.QUERY_3.getCopy());
        supportReplies.add(TestReplies.REPLY_1.getCopy());
        queries.add(TestQueries.QUERY_2.getCopy());
        Integer averageTime1 = TestReplies.REPLY_1.getReply().getWaitingTime();
        Integer averageTime2 = (TestReplies.REPLY_1.getReply().getWaitingTime()
                + TestReplies.REPLY_3.getReply().getWaitingTime()) / 2;
        Integer averageTime3 = null;
        Integer averageTime4 = (TestReplies.REPLY_1.getReply().getWaitingTime()
                + TestReplies.REPLY_1.getReply().getWaitingTime() + TestReplies.REPLY_3.getReply().getWaitingTime())
                / 3;
        expectedStatistics.add(averageTime1);
        expectedStatistics.add(averageTime2);
        expectedStatistics.add(averageTime3);
        expectedStatistics.add(averageTime4);

        List<Integer> actualStatistics = analyzer.analyze(supportReplies, queries);

        assertArrayEquals(expectedStatistics.toArray(), actualStatistics.toArray());
    }

}
