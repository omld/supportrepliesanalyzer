package ua.omld.qu.task.util;

import java.util.regex.Pattern;

/**
 * Utility class with patterns for parsing input data.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/30
 */
public class DataPatterns {
    public static final String DATE_PATTERN_STRING = "(0[1-9]|[12]\\d|3[01])\\.(0?[1-9]|1[0-2])\\.((?:19|20)\\d{2})";

    public static final Pattern QUESTION_PATTERN = Pattern
            .compile("^(?:\\*|(10|[1-9])(?:\\.(20|10|1?[1-9])(?:\\.([1-3]))?)?)$");
    public static final Pattern SERVICE_PATTERN = Pattern.compile("^(?:\\*|(10|[1-9])(?:\\.([1-3]))?)$");
    public static final Pattern DATE_PATTERN = Pattern.compile("^" + DATE_PATTERN_STRING + "$");
    public static final Pattern DATES_RANGE_PATTERN = Pattern
            .compile("^" + DATE_PATTERN_STRING + "(?:\\-(" + DATE_PATTERN_STRING + "))?$");
    public static final Pattern TIME_PATTERN = Pattern.compile("^\\d{1,6}$");

    private DataPatterns() {
    }
}
