package ua.omld.qu.task.util;

import static ua.omld.qu.task.util.DataConstants.DATES_DELIMITER;
import static ua.omld.qu.task.util.DataConstants.DATE_PATTERN_STRING;
import static ua.omld.qu.task.util.DataConstants.ELEMENTS_DELIMITER;
import static ua.omld.qu.task.util.DataConstants.MAX_LINES;
import static ua.omld.qu.task.util.DataConstants.QUERY_DATES_INDEX;
import static ua.omld.qu.task.util.DataConstants.QUERY_ELEMENTS;
import static ua.omld.qu.task.util.DataConstants.QUERY_QUESTION_INDEX;
import static ua.omld.qu.task.util.DataConstants.QUERY_SERVICE_INDEX;
import static ua.omld.qu.task.util.DataConstants.QUERY_TYPE_INDEX;
import static ua.omld.qu.task.util.DataConstants.QUESTION_CATEGORY_GROUP;
import static ua.omld.qu.task.util.DataConstants.QUESTION_SUBCATEGORY_GROUP;
import static ua.omld.qu.task.util.DataConstants.QUESTION_TYPE_GROUP;
import static ua.omld.qu.task.util.DataConstants.REPLY_DATE_INDEX;
import static ua.omld.qu.task.util.DataConstants.REPLY_ELEMENTS;
import static ua.omld.qu.task.util.DataConstants.REPLY_QUESTION_INDEX;
import static ua.omld.qu.task.util.DataConstants.REPLY_SERVICE_INDEX;
import static ua.omld.qu.task.util.DataConstants.REPLY_TIME_INDEX;
import static ua.omld.qu.task.util.DataConstants.REPLY_TYPE_INDEX;
import static ua.omld.qu.task.util.DataConstants.SERVICE_ID_GROUP;
import static ua.omld.qu.task.util.DataConstants.SERVICE_VARIATION_GROUP;
import static ua.omld.qu.task.util.DataConstants.WILDCARD;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.regex.Matcher;

import ua.omld.qu.task.exception.DataReaderException;
import ua.omld.qu.task.model.Query;
import ua.omld.qu.task.model.Question;
import ua.omld.qu.task.model.RecordType;
import ua.omld.qu.task.model.ResponseType;
import ua.omld.qu.task.model.Service;
import ua.omld.qu.task.model.SupportReply;

/**
 * DataReader read data from input stream and parse it to objects.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/04/02
 */
public class DataReader implements Closeable {

    private static final String ERROR_BAD_DATE = "Date can't be parsed.";
    private static final String ERROR_BAD_LINES_COUNT = "Bad lines count.";
    private static final String ERROR_BAD_QUESTION = "Question can't be parsed.";
    private static final String ERROR_BAD_SERVICE = "Service can't be parsed.";
    private static final String ERROR_BAD_TIME = "Time can't be parsed.";
    private static final String ERROR_COUNT_OUT_OF_BOUNDS = "Invalid count: expected: 0 < i <= " + MAX_LINES
            + "; actual: %s";
    private static final String ERROR_EMPTY_LINE = "Line is empty.";
    private static final String ERROR_NOT_PARSED = "Error: data not parsed yet.";
    private static final String ERROR_READ_DATA = "Error read line: %d, cause: %s";
    private static final String READER_MUST_BE_PROVIDED = "Reader must be provided.";

    private BufferedReader reader;
    boolean dataParsed;
    private NavigableSet<SupportReply> supportReplies;
    private NavigableSet<Query> queries;

    /**
     * Constructs {@code DataReader}
     * 
     * @param reader reader for character stream
     * @throws IllegalArgumentException if reader is not provided or has bad size
     */
    public DataReader(Reader reader) {
        super();
        if (reader == null) {
            throw new IllegalArgumentException(READER_MUST_BE_PROVIDED);
        }
        this.reader = new BufferedReader(reader);
        supportReplies = new TreeSet<>();
        queries = new TreeSet<>();
    }

    /**
     * Return parsed support replies.
     * 
     * @throws DataReaderException if data not yet parsed
     * @return parsed support replies
     */
    public NavigableSet<SupportReply> getParsedSupportReplies() {
        if (!dataParsed) {
            throw new DataReaderException(ERROR_NOT_PARSED);
        }
        return supportReplies;
    }

    /**
     * Return parsed queries.
     * 
     * @throws DataReaderException if data not yet parsed
     * @return parsed queries
     */
    public NavigableSet<Query> getParsedQueries() {
        if (!dataParsed) {
            throw new DataReaderException(ERROR_NOT_PARSED);
        }
        return queries;
    }

    /**
     * Parse data from input stream and store it in inner variables.<br>
     * Call {@link #getParsedSupportReplies()} and {@link #getParsedQueries()} to
     * retrieve parsed data.
     * 
     * @throws IndexOutOfBoundsException if number for lines count provided on first
     *                                   line is not in allowed range
     * @throws DataReaderException       on parsing errors
     */
    public void parseData() {
        int linesCount = 0;
        try {
            linesCount = Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException formatException) {
            throw new DataReaderException(ERROR_BAD_LINES_COUNT);
        }
        checkLinesCount(linesCount);
        readAndParseData(linesCount);
        dataParsed = true;
    }

    private void checkLinesCount(int count) {
        if (count <= 0 && count > MAX_LINES) {
            throw new IndexOutOfBoundsException(String.format(ERROR_COUNT_OUT_OF_BOUNDS, count));
        }
    }

    private void readAndParseData(int count) {
        int i = 1;
        try {
            for (; i <= count; i++) {
                String line = readLine();
                parseLine(line);
            }
        } catch (IOException | ParseException | IllegalArgumentException ex) {
            dataParsed = false;
            supportReplies.clear();
            queries.clear();
            throw new DataReaderException(String.format(ERROR_READ_DATA, i, ex.getMessage()), ex);
        }
    }

    private String readLine() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            throw new EOFException(ERROR_EMPTY_LINE);
        }
        return line;
    }

    private void parseLine(String line) throws ParseException {
        String[] lineElements = line.split(ELEMENTS_DELIMITER);
        RecordType lineType = RecordType.fromId(line.charAt(0));
        if (lineType == RecordType.SUPPORT_REPLY) {
            parseSupportReply(lineElements);
        } else if (lineType == RecordType.QUERY) {
            parseQuery(lineElements);
        } else {
            throw new ParseException("Invalid line type", 0);
        }
    }

    private void parseSupportReply(String[] supportReplyStrings) throws ParseException {
        validateSupportReply(supportReplyStrings);
        SupportReply supportReply = new SupportReply(
                parseService(supportReplyStrings[REPLY_SERVICE_INDEX], false),
                parseQuestion(supportReplyStrings[REPLY_QUESTION_INDEX], false),
                parseResponseType(supportReplyStrings[REPLY_TYPE_INDEX]),
                parseDate(supportReplyStrings[REPLY_DATE_INDEX]), 
                parseTime(supportReplyStrings[REPLY_TIME_INDEX]));
        supportReplies.add(supportReply);
    }

    private void validateSupportReply(String[] supportReply) {
        ValidationUtils.validateArrayLength(supportReply, REPLY_ELEMENTS);
        ValidationUtils.validateServiceDescriptor(supportReply[REPLY_SERVICE_INDEX]);
        ValidationUtils.validateQuestionDescriptor(supportReply[REPLY_QUESTION_INDEX]);
        ValidationUtils.validateResponseDescriptor(supportReply[REPLY_TYPE_INDEX]);
        ValidationUtils.validateDateDescriptor(supportReply[REPLY_DATE_INDEX]);
        ValidationUtils.validateTimeDescriptor(supportReply[REPLY_TIME_INDEX]);
    }

    private Service parseService(String serviceString, boolean wildcardAllowed) throws ParseException {
        Matcher matcher = DataPatterns.SERVICE_PATTERN.matcher(serviceString);
        matcher.matches();
        if (!wildcardAllowed && matcher.group().equals(WILDCARD)) {
            throw new ParseException(ERROR_BAD_SERVICE, 0);
        }
        Integer id = parseInteger(matcher.group(SERVICE_ID_GROUP), ERROR_BAD_SERVICE, SERVICE_ID_GROUP);
        Integer variation = parseInteger(matcher.group(SERVICE_VARIATION_GROUP), ERROR_BAD_SERVICE,
                SERVICE_VARIATION_GROUP);
        return Service.getService(id, variation);
    }

    private Integer parseInteger(String integerString, String errorString, int elementIndex) throws ParseException {
        if (integerString == null) {
            return null;
        }
        try {
            return Integer.valueOf(integerString);
        } catch (NumberFormatException e) {
            throw new ParseException(errorString, elementIndex);
        }
    }

    private Question parseQuestion(String questionString, boolean wildcardAllowed) throws ParseException {
        Matcher matcher = DataPatterns.QUESTION_PATTERN.matcher(questionString);
        matcher.matches();
        if (!wildcardAllowed && matcher.group().equals(WILDCARD)) {
            throw new ParseException(ERROR_BAD_QUESTION, 0);
        }
        Integer type = parseInteger(matcher.group(QUESTION_TYPE_GROUP), ERROR_BAD_QUESTION, QUESTION_TYPE_GROUP);
        Integer category = parseInteger(matcher.group(QUESTION_CATEGORY_GROUP), ERROR_BAD_QUESTION,
                QUESTION_CATEGORY_GROUP);
        Integer subcategory = parseInteger(matcher.group(QUESTION_SUBCATEGORY_GROUP), ERROR_BAD_QUESTION,
                QUESTION_SUBCATEGORY_GROUP);
        return Question.getQuestion(type, category, subcategory);
    }

    private ResponseType parseResponseType(String responseString) throws ParseException {
        ResponseType responseType;
        try {
            responseType = ResponseType.fromId(responseString.charAt(0));
        } catch (IllegalArgumentException illegalArgumentException) {
            throw new ParseException(illegalArgumentException.getMessage(), 0);
        }
        return responseType;
    }

    private LocalDate parseDate(String dateString) throws ParseException {
        try {
            return LocalDate.parse(dateString, DateTimeFormatter.ofPattern(DATE_PATTERN_STRING));
        } catch (Exception e) {
            throw new ParseException(ERROR_BAD_DATE, 0);
        }
    }

    private Integer parseTime(String timeString) throws ParseException {
        try {
            return Integer.parseInt(timeString);
        } catch (Exception e) {
            throw new ParseException(ERROR_BAD_TIME, REPLY_TIME_INDEX);
        }
    }

    private void parseQuery(String[] inputQueryStrings) throws ParseException {
        validateQuery(inputQueryStrings);
        String[] datesStrings = inputQueryStrings[QUERY_DATES_INDEX].split(DATES_DELIMITER);
        LocalDate endDate = datesStrings.length == 1 ? null : parseDate(datesStrings[1]);
        Query query = new Query(
                parseService(inputQueryStrings[QUERY_SERVICE_INDEX], true),
                parseQuestion(inputQueryStrings[QUERY_QUESTION_INDEX], true),
                parseResponseType(inputQueryStrings[QUERY_TYPE_INDEX]), 
                parseDate(datesStrings[0]), endDate);
        queries.add(query);
    }

    private void validateQuery(String[] query) {
        ValidationUtils.validateArrayLength(query, QUERY_ELEMENTS);
        ValidationUtils.validateServiceDescriptor(query[QUERY_SERVICE_INDEX]);
        ValidationUtils.validateQuestionDescriptor(query[QUERY_QUESTION_INDEX]);
        ValidationUtils.validateResponseDescriptor(query[QUERY_TYPE_INDEX]);
        ValidationUtils.validateDatesRangeDescriptor(query[QUERY_DATES_INDEX]);
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }

}
