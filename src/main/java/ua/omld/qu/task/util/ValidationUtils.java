package ua.omld.qu.task.util;

import java.util.regex.Pattern;

import ua.omld.qu.task.model.ResponseType;

/**
 * Utility class for validating input text data.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/04/02
 */
public class ValidationUtils {

    private static final String BAD_ELEMENTS_COUNT = "Count of elements is wrong.";
    private static final String WRONG_DATE_DESCRIPTOR = "Element is not a date.";
    private static final String WRONG_DATES_RANGE_DESCRIPTOR = "Element is not a dates range.";
    private static final String WRONG_QUESTION_DESCRIPTOR = "Element is not a question.";
    private static final String WRONG_RESPONSE_DESCRIPTOR = "Element is not a response.";
    private static final String WRONG_SERVICE_DESCRIPTOR = "Element is not a service.";
    private static final String WRONG_TIME_DESCRIPTOR = "Element is not a time.";

    private ValidationUtils() {
    }

    public static <T> void validateArrayLength(T[] array, int allowedLength) {
        if (array.length != allowedLength) {
            throw new IllegalArgumentException(BAD_ELEMENTS_COUNT);
        }
    }

    public static void validateDateDescriptor(String dateDescriptor) {
        validateStringByPattern(DataPatterns.DATE_PATTERN, dateDescriptor, WRONG_DATE_DESCRIPTOR);
    }

    private static void validateStringByPattern(Pattern pattern, String descriptor, String message) {
        if (!pattern.matcher(descriptor).matches()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateDatesRangeDescriptor(String datesRangeDescriptor) {
        validateStringByPattern(DataPatterns.DATES_RANGE_PATTERN, datesRangeDescriptor, WRONG_DATES_RANGE_DESCRIPTOR);
    }

    public static void validateQuestionDescriptor(String questionDescriptor) {
        validateStringByPattern(DataPatterns.QUESTION_PATTERN, questionDescriptor, WRONG_QUESTION_DESCRIPTOR);
    }

    public static void validateResponseDescriptor(String responseDescriptor) {
        try {
            ResponseType.fromId(responseDescriptor.charAt(0));
        } catch (Exception e) {
            throw new IllegalArgumentException(WRONG_RESPONSE_DESCRIPTOR);
        }
    }

    public static void validateServiceDescriptor(String serviceDescriptor) {
        validateStringByPattern(DataPatterns.SERVICE_PATTERN, serviceDescriptor, WRONG_SERVICE_DESCRIPTOR);
    }

    public static void validateTimeDescriptor(String timeDescriptor) {
        validateStringByPattern(DataPatterns.TIME_PATTERN, timeDescriptor, WRONG_TIME_DESCRIPTOR);
    }

}
