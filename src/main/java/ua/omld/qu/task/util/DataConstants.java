package ua.omld.qu.task.util;

public class DataConstants {

    public static final int MAX_LINES = 100_000;
    public static final String DATE_PATTERN_STRING = "dd.MM.yyyy";
    public static final String DATES_DELIMITER = "-";
    public static final String ELEMENTS_DELIMITER = " ";
    public static final String WILDCARD = "*";

    public static final int REPLY_ELEMENTS = 6;
    public static final int REPLY_SERVICE_INDEX = 1;
    public static final int REPLY_QUESTION_INDEX = 2;
    public static final int REPLY_TYPE_INDEX = 3;
    public static final int REPLY_DATE_INDEX = 4;
    public static final int REPLY_TIME_INDEX = 5;
    
    public static final int QUERY_ELEMENTS = 5;
    public static final int QUERY_SERVICE_INDEX = 1;
    public static final int QUERY_QUESTION_INDEX = 2;
    public static final int QUERY_TYPE_INDEX = 3;
    public static final int QUERY_DATES_INDEX = 4;
    
    public static final int SERVICE_ID_GROUP = 1;
    public static final int SERVICE_VARIATION_GROUP = 2;
    
    public static final int QUESTION_TYPE_GROUP = 1;
    public static final int QUESTION_CATEGORY_GROUP = 2;
    public static final int QUESTION_SUBCATEGORY_GROUP = 3;

    private DataConstants() {
    }

}
