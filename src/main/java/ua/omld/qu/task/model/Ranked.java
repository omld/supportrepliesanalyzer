package ua.omld.qu.task.model;

/**
 * Class that helps arranging elements in NavigableSet.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/04/09
 */
public abstract class Ranked implements Comparable<Ranked> {

    private static int lastRank = 0;
    private int rank;

    public Ranked() {
        super();
        this.rank = ++lastRank;
    }

    public Ranked(int rank) {
        super();
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }

    @Override
    public int compareTo(Ranked o) {
        return Integer.compare(rank, o.rank);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + rank;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Ranked other = (Ranked) obj;
        return rank == other.rank;
    }

}
