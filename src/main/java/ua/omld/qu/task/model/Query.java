package ua.omld.qu.task.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Represents query for analysis support replies.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/27
 */
public class Query extends Ranked {

    private Service service;
    private Question question;
    private ResponseType responseType;
    private LocalDate startDate;
    private LocalDate endDate;

    public Query(Service service, Question question, ResponseType responseType, LocalDate startDate,
            LocalDate endDate) {
        super();
        this.service = service;
        this.question = question;
        this.responseType = responseType;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Service getService() {
        return service;
    }

    public Question getQuestion() {
        return question;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * Returns a hash code value for the object.<br>
     * <p>
     * This method does not takes to account parent Ranked class state.
     * 
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hash(endDate, question, responseType, service, startDate);
        return result;
    }

    /**
     * Indicates whether some other {@code Query} object is "equal to" this one.<br>
     * <p>
     * This method does not takes to account parent Ranked class state.
     * 
     * @return a hash code value for this object.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Query other = (Query) obj;
        return Objects.equals(endDate, other.endDate) 
                && Objects.equals(question, other.question)
                && responseType == other.responseType 
                && Objects.equals(service, other.service)
                && Objects.equals(startDate, other.startDate);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Query [service=").append(service)
                .append(", question=").append(question)
                .append(", responseType=").append(responseType)
                .append(", startDate=").append(startDate)
                .append(", endDate=").append(endDate)
                .append("]");
        return builder.toString();
    }

}
