package ua.omld.qu.task.model;

import static ua.omld.qu.task.model.ServiceConstants.MAX_SERVICE_ID;
import static ua.omld.qu.task.model.ServiceConstants.MAX_SERVICE_VARIATION;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents service provided by company.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/26
 */
public final class Service {

    private static final int MATCH_ALL_VALUE = 0;

    private static final String MATCH_ALL_STRING = "*";

    private static final String ID_STRING = "id";
    private static final String VARIATION_STRING = "variation";

    private static final String RANGE_ERROR = "Service %s must be in the range [%d, %d].";

    private static final Map<Integer, Service> SERVICES = new HashMap<>();

    private final int hashValue;
    private final int id;
    private final int variation;

    private Service(int id, int idVariation, int hashValue) {
        super();
        this.id = id;
        this.variation = idVariation;
        this.hashValue = hashValue;
    }

    public int getId() {
        return id;
    }

    public int getVariation() {
        return variation;
    }

    /**
     * Returns {@code Service} that match any id and variation id.
     * 
     * @return service
     */
    public static Service getService() {
        return getService(null, null);
    }

    /**
     * Returns {@code Service} with specified id and has no variation.<br>
     * If null provided as parameter, {@code Service} with no id will be created.
     * 
     * @param id service id
     * @throws IllegalArgumentException on bad parameters
     * @return service
     */
    public static Service getService(Integer id) {
        return getService(id, null);
    }

    /**
     * Returns {@code Service} with specified id and variation.<br>
     * If null provided as parameter, {@code Service} with no variation and/or id
     * will be created.
     * 
     * @param id        service id
     * @param variation service variation id
     * @throws IllegalArgumentException on bad parameters
     * @return service
     */
    public static Service getService(Integer id, Integer variation) {
        checkValue(variation, MAX_SERVICE_VARIATION, VARIATION_STRING, true);
        checkValue(id, MAX_SERVICE_ID, ID_STRING, variation == null);
        final int finalVariation = variation == null ? MATCH_ALL_VALUE : variation;
        final int finalId = id == null ? MATCH_ALL_VALUE : id;
        Integer identity = calculateIdentity(finalId, finalVariation);
        return SERVICES.computeIfAbsent(identity, k -> new Service(finalId, finalVariation, k));
    }

    private static void checkValue(Integer value, int max, String variableName, boolean nullAllowed) {
        if ((!nullAllowed && value == null) || (value != null) && (value < 1 || value > max)) {
            throw new IllegalArgumentException(String.format(RANGE_ERROR, variableName, 1, max));
        }
    }

    private static int calculateIdentity(int id, int variation) {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + variation;
        return result;
    }

    /**
     * Return true if given {@code Service} matches this {@code Service}.<br>
     * It means:<br>
     * <ul>
     *  <li>given {@code Service} match any service;
     *  <li>given {@code Service} has the same type and has no variation;
     *  <li>given {@code Service} has the same type and variation;
     * </ul>
     * 
     * @param other given Service
     * @return true is match
     */
    public boolean isMatch(Service other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        return other.id == MATCH_ALL_VALUE
                || (this.id == other.id && (other.variation == MATCH_ALL_VALUE || this.variation == other.variation));
    }

    @Override
    public int hashCode() {
        return hashValue;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }

    /**
     * Returns a brief description of this service. The exact details of the
     * representation are unspecified and subject to change.
     * 
     * Examples: "Service[*]" "Service[3]" "Service[2.1]"
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Service [");
        if (id == MATCH_ALL_VALUE) {
            builder.append(MATCH_ALL_STRING);
        } else {
            builder.append(id);
            if (variation != MATCH_ALL_VALUE) {
                builder.append(".").append(variation);
            }
        }
        builder.append("]");
        return builder.toString();
    }

}
