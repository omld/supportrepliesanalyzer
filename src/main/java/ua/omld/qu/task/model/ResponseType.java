package ua.omld.qu.task.model;

/**
 * Response type of support reply.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/27
 */
public enum ResponseType {

    FIRST('P'), NEXT('N');

    private static final String BAD_ID = "Bad ResponseType id.";
    private char id;

    private ResponseType(char id) {
        this.id = id;
    }

    public char getId() {
        return id;
    }

    public static ResponseType fromId(char id) {
        for (ResponseType type : ResponseType.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        throw new IllegalArgumentException(BAD_ID);
    }

}
