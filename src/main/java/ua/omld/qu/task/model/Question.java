package ua.omld.qu.task.model;

import static ua.omld.qu.task.model.QuestionConstants.MAX_QUESTION_CATEGORY;
import static ua.omld.qu.task.model.QuestionConstants.MAX_QUESTION_SUBCATEGORY;
import static ua.omld.qu.task.model.QuestionConstants.MAX_QUESTION_TYPE;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents question of a customer.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/26
 */
public final class Question {

    private static final int MATCH_ALL_VALUE = 0;

    private static final String MATCH_ALL_STRING = "*";

    private static final String CATEGORY_STRING = "category";
    private static final String SUBCATEGORY_STRING = "subcategory";
    private static final String TYPE_STRING = "type";

    private static final String RANGE_ERROR = "Question %s must be in the range [%d, %d].";

    private static final Map<Integer, Question> QUESTIONS = new HashMap<>();

    private final int hashValue;
    private final int type;
    private final int category;
    private final int subcategory;

    private Question(int type, int category, int subcategory, int hashValue) {
        super();
        this.type = type;
        this.category = category;
        this.subcategory = subcategory;
        this.hashValue = hashValue;
    }

    public int getType() {
        return type;
    }

    public int getCategory() {
        return category;
    }

    public int getSubcategory() {
        return subcategory;
    }

    /**
     * Return {@code Question} that match any type, category and sub-category id.
     * 
     * @return Question
     */
    public static Question getQuestion() {
        return getQuestion(null, null, null);
    }

    /**
     * Return {@code Question} with specified type and has no category and
     * sub-category.<br>
     * If null provided as parameter, {@code Question} with no type will be created.
     * 
     * @param type Question type id
     * @throws IllegalArgumentException on bad parameters
     * @return Question
     */
    public static Question getQuestion(Integer type) {
        return getQuestion(type, null, null);
    }

    /**
     * Return {@code Question} with specified type, category and has no
     * sub-category.<br>
     * If null provided as parameter, {@code Question} with no category and/or type
     * will be created. Only most latest parameters can be null.
     * 
     * @param type     question type id
     * @param category question category id
     * @throws IllegalArgumentException on bad parameters
     * @return Question
     */
    public static Question getQuestion(Integer type, Integer category) {
        return getQuestion(type, category, null);
    }

    /**
     * Return {@code Question} with specified type, category and sub-category.<br>
     * If null provided as parameter, {@code Question} with no sub-category and/or
     * category and/or type will be created. Only most latest parameters can be
     * null.
     * 
     * @param type        question type id
     * @param category    question category id
     * @param subcategory question sub-category id
     * @throws IllegalArgumentException on bad parameters
     * @return Question
     */
    public static Question getQuestion(Integer type, Integer category, Integer subcategory) {
        checkValue(subcategory, MAX_QUESTION_SUBCATEGORY, SUBCATEGORY_STRING, true);
        checkValue(category, MAX_QUESTION_CATEGORY, CATEGORY_STRING, subcategory == null);
        checkValue(type, MAX_QUESTION_TYPE, TYPE_STRING, category == null);
        final int finalSubcategory = subcategory == null ? MATCH_ALL_VALUE : subcategory;
        final int finalCategory = category == null ? MATCH_ALL_VALUE : category;
        final int finalType = type == null ? MATCH_ALL_VALUE : type;
        Integer identity = calculateIdentity(finalType, finalCategory, finalSubcategory);
        return QUESTIONS.computeIfAbsent(identity, k -> new Question(finalType, finalCategory, finalSubcategory, k));
    }

    private static void checkValue(Integer value, int max, String variableName, boolean nullAllowed) {
        if ((!nullAllowed && value == null) || (value != null) && (value < 1 || value > max)) {
            throw new IllegalArgumentException(String.format(RANGE_ERROR, variableName, 1, max));
        }
    }

    private static int calculateIdentity(int type, int category, int subcategory) {
        final int prime = 31;
        int result = 1;
        result = prime * result + type;
        result = prime * result + category;
        result = prime * result + subcategory;
        return result;
    }

    /**
     * Return true if given {@code Question} matches this Question.<br>
     * It means:
     * <ul>
     * <li>given {@code Question} math any question;
     * <li>given {@code Question} has the same type and has no category and sub-category;
     * <li>given {@code Question} has the same type and category and has no sub-category;
     * <li>given {@code Question} has the same type, category and sub-category;
     * </ul>
     * 
     * @param other given Question
     * @return true is match
     */
    public boolean isMatch(Question other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        return other.type == MATCH_ALL_VALUE
                || (this.type == other.type && (other.category == MATCH_ALL_VALUE || (this.category == other.category
                        && (other.subcategory == MATCH_ALL_VALUE || this.subcategory == other.subcategory))));
    }

    @Override
    public int hashCode() {
        return hashValue;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }

    /**
     * Returns a brief description of this question. The exact details of the
     * representation are unspecified and subject to change.
     * 
     * Examples: "Question[*]" "Question[3]" "Question[2.1]"
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Question [");
        if (type == MATCH_ALL_VALUE) {
            builder.append(MATCH_ALL_STRING);
        } else {
            builder.append(type);
            if (category != MATCH_ALL_VALUE) {
                builder.append(".").append(category);
                if (subcategory != MATCH_ALL_VALUE) {
                    builder.append(".").append(subcategory);
                }
            }
        }
        builder.append("]");
        return builder.toString();
    }
}
