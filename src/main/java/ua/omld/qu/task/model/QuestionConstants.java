package ua.omld.qu.task.model;

public class QuestionConstants {

    public static final int MAX_QUESTION_CATEGORY = 20;
    public static final int MAX_QUESTION_SUBCATEGORY = 5;
    public static final int MAX_QUESTION_TYPE = 10;

    private QuestionConstants() {
    }

}
