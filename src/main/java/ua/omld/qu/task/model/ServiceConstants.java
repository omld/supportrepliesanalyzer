package ua.omld.qu.task.model;

public class ServiceConstants {

    public static final int MAX_SERVICE_ID = 10;
    public static final int MAX_SERVICE_VARIATION = 3;

    private ServiceConstants() {
    }

}
