package ua.omld.qu.task.model;

/**
 * Represent type of file record (line).
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/04/11
 */
public enum RecordType {

    SUPPORT_REPLY('C'), QUERY('D');

    private static final String BAD_ID = "Bad RecordType id.";
    private char id;

    private RecordType(char id) {
        this.id = id;
    }

    public char getId() {
        return id;
    }

    public static RecordType fromId(char id) {
        for (RecordType type : RecordType.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        throw new IllegalArgumentException(BAD_ID);
    }

}
