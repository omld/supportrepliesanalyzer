package ua.omld.qu.task.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Represent support reply - information about customer call.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/27
 */
public class SupportReply extends Ranked {

    private Service service;
    private Question question;
    private ResponseType responseType;
    private LocalDate date;
    private Integer waitingTime;

    /**
     * Construct regular support reply.
     * 
     * @param service      company service
     * @param question     customer question
     * @param responseType company response
     * @param date         date of reply
     * @param waitingTime  time of customer waiting
     */
    public SupportReply(Service service, Question question, ResponseType responseType, LocalDate date,
            Integer waitingTime) {
        super();
        this.service = service;
        this.question = question;
        this.responseType = responseType;
        this.date = date;
        this.waitingTime = waitingTime;
    }

    /**
     * Construct empty Support reply with given rank.
     * 
     * @param rank given rank
     */
    public SupportReply(int rank) {
        super(rank);
        this.service = null;
        this.question = null;
        this.responseType = null;
        this.date = null;
        this.waitingTime = null;
    }

    public Service getService() {
        return service;
    }

    public Question getQuestion() {
        return question;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getWaitingTime() {
        return waitingTime;
    }

    /**
     * Returns a hash code value for the object.<br>
     * <p>
     * This method does not takes to account parent Ranked class state.
     * 
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hash(date, question, responseType, service, waitingTime);
        return result;
    }

    /**
     * Indicates whether some other {@code SupportReply} object is "equal to" this
     * one.<br>
     * <p>
     * This method does not takes to account parent Ranked class state.
     * 
     * @return a hash code value for this object.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SupportReply other = (SupportReply) obj;
        return Objects.equals(date, other.date) 
                && Objects.equals(question, other.question)
                && responseType == other.responseType 
                && Objects.equals(service, other.service)
                && Objects.equals(waitingTime, other.waitingTime);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SupportReply [service=").append(service)
                .append(", question=").append(question)
                .append(", responseType=").append(responseType)
                .append(", date=").append(date)
                .append(", waitingTime=").append(waitingTime)
                .append("]");
        return builder.toString();
    }

}
