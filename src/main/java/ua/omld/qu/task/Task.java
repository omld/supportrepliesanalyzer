package ua.omld.qu.task;

import java.io.FileNotFoundException;
import java.util.logging.Logger;

/**
 * Main class for run task.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/26
 */
public class Task {

    private static final Logger LOGGER = Logger.getLogger(Task.class.getName());
    private static final String ERROR_CREATE_TASK_RUNNER = "Error: can`t make TaskRunner: %n%s%n";
    private static final String WARNING_ONE_ARGUMENT = "Expected only one argument! Found: %d%n";

    public static void main(String[] args) {
        if (args.length > 1) {
            LOGGER.info(String.format(WARNING_ONE_ARGUMENT, args.length));
            System.exit(0);
        }
        try {
            TaskRunner taskRunner;
            if (args.length == 1) {
                taskRunner = new TaskRunner(args[0]);
            } else {
                taskRunner = new TaskRunner();
                System.out.println("Please, provide data:");
            }
            taskRunner.run();
        } catch (FileNotFoundException fileNotFoundException) {
            LOGGER.severe(String.format(ERROR_CREATE_TASK_RUNNER, fileNotFoundException.toString()));
        }
    }

}
