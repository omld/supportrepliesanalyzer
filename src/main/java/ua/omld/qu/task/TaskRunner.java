package ua.omld.qu.task;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;
import java.util.Objects;
import java.util.logging.Logger;

import ua.omld.qu.task.exception.DataReaderException;
import ua.omld.qu.task.logic.Analyzer;
import ua.omld.qu.task.model.Query;
import ua.omld.qu.task.model.SupportReply;
import ua.omld.qu.task.util.DataReader;

/**
 * Provides proper task running and logging of errors.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/03/26
 */
public class TaskRunner {

    private static final Logger LOGGER = Logger.getLogger(TaskRunner.class.getName());

    private static final String ERROR_ANILYZE = "Error: can't analyze data: %n%s%n";
    private static final String ERROR_READ_DATA = "Error: can't read data: %n%s%n";
    private static final String NO_DATA_OUTPUT = "-";

    private InputStream inputStream;
    NavigableSet<SupportReply> supportReplies;
    NavigableSet<Query> queries;
    List<Integer> statistics;

    /**
     * Constructor for using system input stream as data source.
     */
    public TaskRunner() {
        super();
        this.inputStream = System.in;
    }

    /**
     * Constructor for using file as data source.
     */
    public TaskRunner(String filename) throws FileNotFoundException {
        super();
        this.inputStream = new FileInputStream(filename);
    }

    /**
     * Executes operations needed for task.
     */
    public void run() {
        supportReplies = null;
        queries = null;
        readData();
        analyzeData();
        printStatictics(statistics);
    }

    private void readData() {
        try (DataReader reader = new DataReader(new InputStreamReader(inputStream))) {
            reader.parseData();
            supportReplies = reader.getParsedSupportReplies();
            queries = reader.getParsedQueries();
        } catch (DataReaderException | IOException | IllegalArgumentException exception) {
            LOGGER.severe(String.format(ERROR_READ_DATA, exception.getMessage()));
        }
    }

    private void analyzeData() {
        statistics = new ArrayList<>();
        try {
            Analyzer analyzer = new Analyzer();
            statistics = analyzer.analyze(supportReplies, queries);
        } catch (Exception e) {
            LOGGER.severe(String.format(ERROR_ANILYZE, e.getMessage()));
        }
    }

    private void printStatictics(List<Integer> results) {
        if (results == null || results.isEmpty()) {
            System.out.println("Statistics is absent.");
        } else {
            System.out.println("Statistics:");
            for (Integer result : results) {
                if (Objects.equals(result, Analyzer.NO_DATA_VALUE)) {
                    System.out.println(NO_DATA_OUTPUT);
                } else {
                    System.out.println(result);
                }
            }
        }
    }

}
