package ua.omld.qu.task.exception;

/**
 * Exception that accrues during reading and parsing data.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/04/02
 */
public class DataReaderException extends RuntimeException {

    private static final long serialVersionUID = -34267190945136939L;

    public DataReaderException() {
        super();
    }

    public DataReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataReaderException(String message) {
        super(message);
    }

    public DataReaderException(Throwable cause) {
        super(cause);
    }

}
