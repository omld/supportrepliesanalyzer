package ua.omld.qu.task.logic;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.stream.Collectors;

import ua.omld.qu.task.model.Query;
import ua.omld.qu.task.model.SupportReply;

/**
 * Makes analysis of given support replies according to queries.
 * 
 * @author Oleksii Kostetskyi
 * 
 * @since 2019/04/07
 */
public class Analyzer {

    private static final String ERROR_NO_DATA = "Data for analysis is absent.";

    public static final Integer NO_DATA_VALUE = null;

    public Analyzer() {
        super();
    }

    public List<Integer> analyze(NavigableSet<SupportReply> supportReplies, NavigableSet<Query> queries) {
        checkData(supportReplies);
        checkData(queries);
        return calculateStatistics(supportReplies, queries);
    }

    private <T> void checkData(NavigableSet<T> data) {
        if (data == null || data.isEmpty()) {
            throw new IllegalArgumentException(ERROR_NO_DATA);
        }
    }

    private List<Integer> calculateStatistics(NavigableSet<SupportReply> supportReplies, NavigableSet<Query> queries) {
        List<Integer> statistics = new ArrayList<>(queries.size());
        for (Query query : queries) {
            List<SupportReply> matchedReplies = new ArrayList<>(queries.size());
            SortedSet<SupportReply> previousReplies = supportReplies.headSet(new SupportReply(query.getRank()));
            for (SupportReply reply : previousReplies) {
                if (checkReplyMatchesQuery(reply, query)) {
                    matchedReplies.add(reply);
                }
            }
            statistics.add(matchedReplies.isEmpty() ? NO_DATA_VALUE : calculateQueryStatistics(matchedReplies));
        }
        return statistics;
    }

    private Integer calculateQueryStatistics(List<SupportReply> matchedReplies) {
        IntSummaryStatistics statistics = matchedReplies.stream()
                .collect(Collectors.summarizingInt(SupportReply::getWaitingTime));
        return Math.round((float) statistics.getAverage());
    }

    private boolean checkReplyMatchesQuery(SupportReply reply, Query query) {
        return reply.getService().isMatch(query.getService()) 
                && reply.getQuestion().isMatch(query.getQuestion())
                && reply.getResponseType().equals(query.getResponseType())
                && checkDateMatchesRange(reply.getDate(), query.getStartDate(), query.getEndDate());
    }

    private boolean checkDateMatchesRange(LocalDate date, LocalDate rangeStart, LocalDate rangeEnd) {
        return date.equals(rangeStart) || date.equals(rangeEnd)
                || (date.isAfter(rangeStart) && date.isBefore(rangeEnd));
    }

}
