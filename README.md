## Support Replies Analyzer ##
Utility for calculation statistics on information about customer support replies

### Task ###
Read and analyze input data: support replies and queries. 

##### Input #####
First line of input contains total amount of lines (<= 100'000).  
Each next line of input represent one of the entities: support reply (begins with 'C') or query for analysis (begins with 'D').


Lines patterns:

~~~~
C service\_id[.variation\_id] question\_type\_id[.category\_id.[sub-category\_id]] P/N date time
D service\_id[.variation\_id] question\_type\_id[.category\_id.[sub-category\_id]] P/N date\_from[-date\_to]
~~~~
Values in square brackets are optional. Company provide 10 different services, each with 3 variations. Questions are divided into 10 types, each can belong to 20 categories, a category can have 5 sub-categories. Response type: ‘P’ (first answer) or ‘N’ (next answer). Response date format is DD.MM.RRRR (27.11.2012). Time in minutes represents customer waiting time.


For query lines, __service_id__ and __question_type__ can have special value “\*”, it means query match all services/question types. In case of value “\*”, no service variation nor service category/sub-category can be specified.

##### Output #####
Query lines give instructions for analysis: average waiting time rounded to minutes for appropriate **service** and **question** must be calculated. Only matching support reply lines defined before query line are counted. If there are no support replies which match given query then “-” must be printed out.

#### Example ####
##### Input #####
7  
C 1.1 8.15.1 P 15.10.2012 83  
C 1 10.1 P 01.12.2012 65  
C 1.1 5.5.1 P 01.11.2012 117  
D 1.1 8 P 01.01.2012-01.12.2012  
C 3 10.2 N 02.10.2012 100  
D 1 * P 8.10.2012-20.11.2012  
D 3 10 P 01.12.2012  
##### Output #####
83  
100  
\-  

#### Instructions ####

To compile application and run tests execute command:

~~~~
 mvn clean verify
~~~~
To make only compilation execute command:

~~~
 mvn clean compile
~~~
You may pass name of file with information (count, support replies and queries) as parameter.  
If you run application without parameters you must provide information on standard input.
